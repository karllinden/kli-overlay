kli-overlay
===========

This overlay contains ebuilds for:
 * software that I write,
 * software I use which is not in the gentoo main tree.
