# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="6"

[[ ${PV} = 9999 ]] && inherit git-r3

if [[ ${PV} = 9999 ]]; then
	SRC_URI=""
	EGIT_REPO_URI="https://gitlab.com/karllinden/${PN}.git"
	KEYWORDS=""
else
	SRC_URI="https://gitlab.com/karllinden/${PN}/raw/releases/${P}.tar.xz"
	KEYWORDS="~amd64 ~x86"
fi
RESTRICT="mirror"

DESCRIPTION="a script that automates updating the waf build system"
HOMEPAGE="https://gitlab.com/karllinden/wafupdate"

LICENSE="GPL-3"
SLOT="0"
IUSE=""

RDEPEND="
	app-crypt/gnupg
	net-misc/wget"

src_install() {
	dobin ${PN}
	dodoc README
}
