# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="6"

PYTHON_COMPAT=( python{2_7,3_5,3_6} )
PYTHON_REQ_USE='threads(+)'
inherit python-any-r1 waf-utils

DESCRIPTION="A utility to create tests, examples and documentation from \
combined source files"
HOMEPAGE="https://gitlab.com/karllinden/cotesdex"

SRC_URI="${HOMEPAGE}/raw/releases/${P}.tar.bz2"
KEYWORDS="~amd64 ~x86"

RESTRICT="mirror"

LICENSE="GPL-3"
SLOT="0"
IUSE="nls valgrind"

RDEPEND="
	dev-libs/gop:4
	valgrind? ( dev-util/valgrind )"
DEPEND="
	virtual/pkgconfig"

myuse() {
	echo $(usex $1 --$1 --no-$1)
}

src_configure() {
	waf-utils_src_configure \
		$(myuse nls)
}

src_test() {
	"${S}"/waf check || die "waf check failed"
}
