# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="6"

if [[ "${PV}" = 9999 ]]; then
	inherit git-r3
fi
PYTHON_COMPAT=(python{2_7,3_4,3_5,3_6})
PYTHON_REQ_USE="threads(+)"
inherit python-any-r1 waf-utils

RESTRICT="mirror"

if [[ "${PV}" = "9999" ]]; then
	EGIT_REPO_URI="https://gitlab.com/karllinden/${PN}.git"
	KEYWORDS=""
else
	SRC_URI="https://gitlab.com/karllinden/${PN}/raw/releases/${P}.tar.bz2"
	KEYWORDS="~amd64 ~x86"
fi

DESCRIPTION="a set of utilities to manage a prefix based developement
environment"
HOMEPAGE="https://gitlab.com/karllinden/kli-env"

LICENSE="GPL-3"
SLOT="0"
IUSE="debug"

RDEPEND="dev-libs/gop:4"
DEPEND="${RDEPEND}
	virtual/pkgconfig"

src_configure() {
	mywafargs=(
		--no-optimize
		$(usex debug --enable-assert "")
	)
	waf-utils_src_configure ${mywafargs[@]}
}
