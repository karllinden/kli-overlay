# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="6"

inherit git-r3

EGIT_REPO_URI="https://gitlab.com/karllinden/${PN}.git"
SRC_URI=""
KEYWORDS=""

RESTRICT="mirror"

DESCRIPTION="A shell script to easily update gentoo systems"
HOMEPAGE="https://gitlab.com/karllinden/kli-update"

LICENSE="GPL-3"
SLOT="0"

RDEPEND="app-admin/sudo
	app-portage/gentoolkit
	app-portage/portage-utils
	app-portage/smart-live-rebuild"

src_install() {
	default
	dobin kli-update
}
