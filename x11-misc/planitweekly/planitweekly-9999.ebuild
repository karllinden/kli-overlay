# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="6"

PYTHON_COMPAT=(python{3_4,3_5,3_6})
inherit gnome2-utils python-single-r1 git-r3

EGIT_REPO_URI="https://gitlab.com/karllinden/${PN}.git"
SRC_URI=""
KEYWORDS=""

RESTRICT="mirror"

DESCRIPTION="A week planner"
HOMEPAGE="https://gitlab.com/karllinden/planitweekly"

LICENSE="GPL-3"
SLOT="0"
IUSE=""

RDEPEND="dev-python/PyQt4
	x11-themes/hicolor-icon-theme"
DEPEND="${RDEPEND}"

src_install() {
	emake DESTDIR="${D}" PREFIX="${EPREFIX}"/usr install
	einstalldocs
}

pkg_postinst() {
	gnome2_icon_cache_update
}

pkg_postrm() {
	gnome2_icon_cache_update
}
