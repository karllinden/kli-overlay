# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="6"

PYTHON_COMPAT=( python{3_5,3_6} )
PYTHON_REQ_USE='threads(+)'
inherit python-any-r1 waf-utils

DESCRIPTION="Great Option Parsing - a C library to parse command line options"
HOMEPAGE="https://gitlab.com/karllinden/gop"

SRC_URI="${HOMEPAGE}/raw/releases/${P}.tar.bz2"
KEYWORDS="~amd64 ~x86"

RESTRICT="mirror"

LICENSE="GPL-3"
SLOT="4"
IUSE="doc examples nls static-libs test"

DEPEND="
	${PYTHON_DEPS}
	virtual/pkgconfig
	doc? (
		app-text/asciidoc
		dev-util/source-highlight
		media-gfx/graphviz
	)
	test? (
		dev-util/valgrind
	)"

PDEPEND="examples? ( app-doc/gop-cotesdex:${SLOT}[examples] )"

myuse() {
	echo $(usex $1 --$1 --no-$1)
}

src_configure() {
	waf-utils_src_configure \
		$(myuse doc) \
		$(myuse nls) \
		$(myuse static-libs)
}

src_test() {
	"${S}"/waf check || die "waf check failed"
}
