# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="6"

PYTHON_COMPAT=( python{2_7,3_5,3_6} )
PYTHON_REQ_USE='threads(+)'
inherit python-any-r1 waf-utils

DESCRIPTION="Tests, documentation and examples for dev-libs/gop"
HOMEPAGE="https://gitlab.com/karllinden/gop-cotesdex"

SRC_URI="${HOMEPAGE}/raw/releases/${P}.tar.bz2"
KEYWORDS="~amd64 ~x86"

RESTRICT="mirror"

LICENSE="GPL-3"
SLOT="4"
IUSE="doc examples valgrind"

DEPEND="
	dev-libs/gop:${SLOT}
	>=dev-util/cotesdex-1.0.0
	virtual/pkgconfig
	doc? (
		app-text/asciidoc
		dev-util/source-highlight
	)
	valgrind? (
		dev-util/valgrind
	)"

myuse() {
	echo $(usex $1 --$1 --no-$1)
}

src_configure() {
	COTESDEXFLAGS=$(usex valgrind --memcheck "") \
		waf-utils_src_configure \
			$(myuse doc) \
			$(myuse examples) \
			--docdir="${EPREFIX}"/usr/share/doc/${PF} \
			--htmldir="${EPREFIX}"/usr/share/doc/${PF}/html
}

src_test() {
	"${S}"/waf check || die "waf check failed"
}
